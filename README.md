# music

Historic archive of incomplete and unfinished sketches written by myself.

In the unlikely event you are using one of the files in this repository ask for
the full finished version, as these are mostly skethes and not actual
compositions. Every file and creative work in this repository is
my intellectual property but you are free to use them as you please as long as
you credit me by linking to this repo.

Contact:
lhernandezcano [at] hotmail [dotcom]