# Gog (Good Old Compositions)

Most of these I wrote using LMMS (Linux Multimedia Studio) in the *secundaria*
and *prepa*. Some are written and audio-rendered in Finale or Guitar Pro before
I switched to Musescore for engraving, but as I lost the original PDFs I
rendered them using Musescore.

Speaking of musescore, I started using it in 2014 and as of 2019 is still my
favorite engraver.
